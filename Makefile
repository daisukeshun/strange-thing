LIBSFOLDER=libs
OBJFOLDER=obj
SRCFOLDER=src
BINFOLDER=bin
INCLUDEFOLDER=include
HEADES=*.h
CC=clang
CFLAGS=-pipe -Wall -W -fPIC
RFLAGS=-pipe -O2 -Wall -W -fPIC
OUTPUT=FGgame
STD=-std=gnu99
LIBS=\
	-lm\
	-ldl\
	-framework OpenGL\

LINKLIBS=\
	-larray\
	-lgame\
	-llogger\
	-lsystems\

SOURCES=\
	array.c\
	game.c\
	logger.c\
	systems.c\

PRECOMPILEDOBS=\
	array.o\
	game.o\
	logger.o\
	systems.o\

PRECOMPILEDLIBS=\
	libarray.a\
	libgame.a\
	liblogger.a\
	libsystems.a\

dev: main.o
	$(CC) -o $(BINFOLDER)/$(OUTPUT) ./$(OBJFOLDER)/$< $(LINKLIBS) $(LIBS) $(CFLAGS) -L $(LIBSFOLDER)

release: main.o
	$(CC) -o $(BINFOLDER)/$(OUTPUT) ./$(OBJFOLDER)/$< $(LINKLIBS) $(LIBS) $(CFLAGS) -L $(LIBSFOLDER)

test: ./$(BINFOLDER)/$(OUTPUT)
	clear
	./$<

clear:
	rm -rf *.o *.a *.gch ./$(LIBSFOLDER)/* ./$(OBJFOLDER)/* ./$(BINFOLDER)/*
libs: $(PRECOMPILEDLIBS)

obj: $(PRECOMPILEDOBS)

main.o: $(SRCFOLDER)/main.c
	$(CC) -c -Wall -o $(OBJFOLDER)/$@ $< -I$(INCLUDEFOLDER) $(STD)

array.o: $(SRCFOLDER)/array.c
	$(CC) -fPIC -c -Wall -o $(OBJFOLDER)/$@ $< -I$(INCLUDEFOLDER) $(STD)
libarray.a: array.o
	ar rcs $(LIBSFOLDER)/$@ $(OBJFOLDER)/$^

game.o: $(SRCFOLDER)/game.c
	$(CC) -fPIC -c -Wall -o $(OBJFOLDER)/$@ $< -I$(INCLUDEFOLDER) $(STD)
libgame.a: game.o
	ar rcs $(LIBSFOLDER)/$@ $(OBJFOLDER)/$^

logger.o: $(SRCFOLDER)/logger.c
	$(CC) -fPIC -c -Wall -o $(OBJFOLDER)/$@ $< -I$(INCLUDEFOLDER) $(STD)
liblogger.a: logger.o
	ar rcs $(LIBSFOLDER)/$@ $(OBJFOLDER)/$^

systems.o: $(SRCFOLDER)/systems.c
	$(CC) -fPIC -c -Wall -o $(OBJFOLDER)/$@ $< -I$(INCLUDEFOLDER) $(STD)
libsystems.a: systems.o
	ar rcs $(LIBSFOLDER)/$@ $(OBJFOLDER)/$^

