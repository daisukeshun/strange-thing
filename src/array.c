#include "array.h"

void * Array2d_Create(ui32 rows, ui32 cols, ui8 size)
{ return calloc(rows*cols, size); }

void * Array_Create(ui64 count, ui8 size)
{ return calloc(count, size); }

string Array_Serialize(array arr, ui64 mem_size)
{
	string out = Array_Create(mem_size + 1, 1);
	memcpy(out, arr, mem_size);
	return out;
}

void Array_Print(array arr, ui64 count, ui8 size)
{
	ui64 i = 0;
	for(i = 0; i < count; i++) {
		if(i % 10 == 0 && i != 0) { printf("\n"); }
		switch(size) {
			case 8:
				printf("%x ", array_element(ui8, arr, i));
				break;
			case 16:
				printf("%x ", array_element(ui16, arr, i));
				break;
			case 32:
				printf("%x ", array_element(ui32, arr, i));
				break;
			case 64:
				printf("%llu ", array_element(ui64, arr, i));
				break;
		}
	}
	printf("\n");
}

void Array2d_Print(array arr, ui32 rows, ui32 cols, ui8 size)
{
	ui64 i = 0, j = 0;
	for(i = 0; i < rows; i++) {
		for(j = 0; j < cols; j++) {
			switch(size) {
				case 8:
					printf("%c ", array2d_element(ui8, arr, rows, cols, i, j));
					break;
				case 16:
					printf("%x ", array2d_element(ui16, arr, rows, cols, i, j));
					break;
				case 32:
					printf("%x ", array2d_element(ui32, arr, rows, cols, i, j));
					break;
				case 64:
					printf("%llu ", array2d_element(ui64, arr, rows, cols, i, j));
					break;
			}
		}
		printf("\n");
	}
	printf("\n");
}

