#include "logger.h"

void GameLog(Game * game_ptr)
{
	Game game = game_ptr[0];
	printf("Game:\n");
	printf("{\n");
	printf("  state:\n");
	printf("  {\n");
	printf("    start:  %d\n", game.state.start);
	printf("    input:  %d\n", game.state.input);
	printf("    update: %d\n", game.state.update);
	printf("    render: %d\n", game.state.render);
	printf("    end:    %d\n", game.state.end);
	printf("  }\n");
	printf("}\n");
}


