#include "game.h"

void Game_Create(Game* game_ptr)
{
	Game game = game_ptr[0];
	game.state.start = true;
	game.state.end = false;
	game.state.input = false;
	game.state.update = false;
	game.state.render = false;
}

void Game_Run()
{
}

void Game_Update()
{
}

void Game_Destroy(Game * game_ptr)
{
	Game game = game_ptr[0];
	game.state.end = true;
	game.state.start = false;
	game.state.input = false;
	game.state.update = false;
	game.state.render = false;
}

