# strange thing

For development need some libs.
Project setup for Linux distros.

<details>
<summary>Ubuntu</summary>

`sudo apt install freeglut3-dev clang make nodejs npm`

</details>

By default in build.sh threads number is 4.
If u can preserve more threads for compiling u should change values after `-j` flag in [build.sh](https://gitlab.com/daisukeshun/strange-thing/-/blob/main/build.sh)


<details>
<summary>For example code build.sh for 12 threads:</summary>

`make libs -j 12 && make dev -j 12`

</details>



For generating new makefile u should run [makefile.sh](https://gitlab.com/daisukeshun/strange-thing/-/blob/main/makefile.sh)
