#ifndef SF_SYSTEMS_H
#define SF_SYSTEMS_H
#include "defines.h"

void InitSystem(void * data, ui16 size);
void InputSystem(void * data, ui16 size);
void UpdateSystem(void * data, ui16 size);
void RenderSystem(void * data, ui16 size);
void DestroySystem(void * data, ui16 size);

#endif
