#ifndef SF_GAME_H
#define SF_GAME_H

#include "defines.h"

typedef struct Game
{
	struct 
	{
		bool start;
		bool input;
		bool update;
		bool render;
		bool end;
	} 
	state;
	bool keys[256];
} Game;

void Game_Create(Game* game);
void Game_Run();
void Game_Update();
void Game_Destroy(Game* game);




#endif
