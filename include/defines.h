#ifndef SF_DEFINES_H
#define SF_DEFINES_H

#include <inttypes.h>

#ifdef __APPLE__
	#include <OpenGL/gl.h>
#elifdef __linux__
	#include <GL/gl.h>
#endif

#ifdef __ARM_NEON
	#include "sse2neon.h" //m1 intrinsics
#endif
#ifdef __X86_64__
	#include <x86intrin.h> //amd64 intrinsics
#endif

#define array void*
#define string char*

/* booleans */
#define bool GLboolean
#define true GL_TRUE
#define false GL_FALSE

/* ints */
#define i8 GLbyte
#define ui8 GLubyte

#define i16 GLshort
#define ui16 GLushort

#define i32 GLint
#define ui32 GLuint

#define i64 GLint64
#define ui64 GLuint64

#define i128 __m128i
#define ui128 __m128

/*floats */
#define f16 GLhalf
#define f32 GLfloat
#define f64 GLdouble
#define f128 __m128d

#define LOG_ENABLE

#ifdef LOG_ENABLE
	#define CALL_FUNC(function, parameters...) printf("%s(%s)\n", #function, #parameters); (function)(parameters)
#else
	#define CALL_FUNC(function, parameters...) (function)(parameters)
#endif

#endif
