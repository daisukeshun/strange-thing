#ifndef SF_ARRAY_H
#define SF_ARRAY_H

#include "defines.h"

#ifdef __APPLE__
	#include <stdlib.h>
#endif
#ifdef __linux__
	#include <malloc.h>
#endif


#include <stdio.h>
#include <memory.h>

array Array2d_Create(ui32 rows, ui32 cols, ui8 size);
array Array_Create(ui64 count, ui8 size);
void Array_Print(array arr, ui64 count, ui8 elem_size);
void Array2d_Print(array arr, ui32 rows, ui32 cols, ui8 size);
string Array_Serialize(array arr, ui64 mem_size);
void Array_Destroy(array arr);

#define array_element(type, arr, index) (((type*)arr)[index])
#define array2d_element(type, arr, rows, cols, i, j) (((type*)arr)[i * rows + j])

#endif
