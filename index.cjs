'use strict'

const fs = require('fs');
const file = fs.readFileSync('./project_config.json', 'utf8');
const json = JSON.parse(file);

function frameworks() {
	let str = '';
	json.frameworks.forEach(e => {
		str+='\t-framework '+e+'\\\n';
	});
	return str;
}

function libs() {
	let str = "LIBS=\\\n";
	json.libs.forEach(e => {
		str+='\t-l'+e+'\\\n';
	});
	if(json.frameworks.length) {
		str+=frameworks();
	}
	str+='\n'
	return str;
}
function linklibs() {
	let str = "LINKLIBS=\\\n";
	json.src.forEach(e => {
		str+= '\t-l'+e+'\\\n';
	});
	str+='\n'
	return str;
}
function sources() {
	let str = "SOURCES=\\\n";
	json.src.forEach(e => {
		str+= '\t'+e+'.c\\\n';
	});
	str+='\n'
	return str;
}
function precompiledobs() {
	let str = "PRECOMPILEDOBS=\\\n";
	json.src.forEach(e => {
		str+= '\t'+e+'.o\\\n';
	});
	str+='\n'
	return str;
}
function precompiledlibs() {
	let str = "PRECOMPILEDLIBS=\\\n";
	json.src.forEach(e => {
		str+= '\tlib'+e+'.a\\\n';
	});
	str+='\n'
	return str;
}

function dev() {
	return 'dev: ' + json.main + '.o\n\t' +
		'$(CC) -o $(BINFOLDER)/$(OUTPUT) ./$(OBJFOLDER)/$< $(LINKLIBS) $(LIBS) $(CFLAGS) -L $(LIBSFOLDER)\n\n';
}

function release() {
	return 'release: ' + json.main + '.o\n\t' +
		'$(CC) -o $(BINFOLDER)/$(OUTPUT) ./$(OBJFOLDER)/$< $(LINKLIBS) $(LIBS) $(CFLAGS) -L $(LIBSFOLDER)\n\n';
}

function test() {
	return 'test: ./$(BINFOLDER)/$(OUTPUT)\n\t' +
		'clear\n\t./$<\n\n';
}

function targets()
{
	let str = '';
	str += 'main.o: $(SRCFOLDER)/' + json.main + '.c\n\t';
	str += '$(CC) -c -Wall -o $(OBJFOLDER)/$@ $< -I$(INCLUDEFOLDER) $(STD)\n\n';

	json.src.forEach(s => {
		str += s + '.o: $(SRCFOLDER)/' + s + '.c\n\t';
		str+= '$(CC) -fPIC -c -Wall -o $(OBJFOLDER)/$@ $< -I$(INCLUDEFOLDER) $(STD)\n';
		str+= 'lib' + s + '.a: ' + s + '.o\n\t';
		str+= 'ar rcs $(LIBSFOLDER)/$@ $(OBJFOLDER)/$^\n\n';
	});
	return str;
}
let makefile = "";
makefile+='LIBSFOLDER=libs\n';
makefile+='OBJFOLDER=obj\n';
makefile+='SRCFOLDER=src\n';
makefile+='BINFOLDER=bin\n';
makefile+='INCLUDEFOLDER=include\n';
makefile+='HEADES=*.h\n';
makefile += `CC=${json.cc}\n`;
makefile += `CFLAGS=${json.cflags}\n`;
makefile += `RFLAGS=${json.rflags}\n`;
makefile += `OUTPUT=${json.out}\n`
makefile += `STD=${json.std}\n`
makefile += libs();
makefile += linklibs();
makefile += sources();
makefile += precompiledobs();
makefile += precompiledlibs();
makefile += dev();
makefile += release();
makefile += test();
makefile += 'clear:\n\trm -rf *.o *.a *.gch ./$(LIBSFOLDER)/* ./$(OBJFOLDER)/* ./$(BINFOLDER)/*\n';
makefile += 'libs: $(PRECOMPILEDLIBS)\n\n';
makefile += 'obj: $(PRECOMPILEDOBS)\n\n';
makefile += targets();

fs.writeFileSync("./Makefile", makefile, "utf8");


